import 'package:flutter/material.dart';

class ItemContainer extends StatelessWidget {
  final String imagePath;
  final String inputText;
  final String inputPrice;
  final String inputSold;
 
  const ItemContainer({
    super.key,
    required this.imagePath,
    required this.inputText,
    required this.inputPrice,
    required this.inputSold,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(0),
      ),
      child: Column(
        children: [
          Image.asset(
            imagePath,
            height: 130,
            width: double.infinity,
            fit: BoxFit.contain,
          ),
          const SizedBox(height: 15),
          Text(inputText),
          const SizedBox(height: 5),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(inputPrice,style: const TextStyle(fontWeight: FontWeight.bold,color: Colors.red),),
              Text(inputSold,style: TextStyle(color: Colors.grey),)
            ],
          ),
          const SizedBox(height: 15),

        ],
      ),
    );
  }
}
