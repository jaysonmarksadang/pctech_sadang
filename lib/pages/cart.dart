import 'package:flutter/material.dart';
import 'package:sadang_flutter/pages/profile.dart';

class Cart extends StatelessWidget {
  const Cart({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.black,
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>const Profile()));
              },
              child: Container(
                height: 35,
                width: 35,
                decoration: const BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(image: AssetImage('lib/images/profilem.png'),fit: BoxFit.contain)),
              ),
            ),
          )
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          // Your content at the top
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children: [
                Container(
                  height: 150,
                  width: 200,
                  decoration: const BoxDecoration(
                    image: DecorationImage(image:  AssetImage('lib/images/item1.png'),fit: BoxFit.cover)),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children:  [
                    const Text('HyperX Alloy Origins 60\nMechanical Gaming Keyboard'),
                     const SizedBox(height: 10),
                    Row(
                      children: [
                         Container(
                        padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                         decoration: BoxDecoration(color: Colors.black,
                         borderRadius: BorderRadius.circular(18)
                       ),child: const Text('Black',style: TextStyle(color: Colors.white),),
                   ),
                   const SizedBox(width: 15),
                   Container(
                        padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                         decoration: BoxDecoration(color: Colors.black,
                         borderRadius: BorderRadius.circular(18)
                       ),child: const Text('Full Size',style: TextStyle(color: Colors.white),),
                   ),
                      ],
                    ),
                   
                  ],
                ),
                
              ],
            ),
          ),
          Expanded(
            child: Container(
             
            ),
          ),
          Container(
            margin: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text('Price:',style: TextStyle(fontSize: 24),),
                    Text('P 4,990',style: TextStyle(fontSize: 20),)
                  ],
                ),
                Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text('Shipping:',style: TextStyle(fontSize: 24)),
                    Text('P 50',style: TextStyle(fontSize: 20))
                  ],
                ),
                Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text('Total:',style: TextStyle(fontSize: 24)),
                    Text('P 5,040',style: TextStyle(fontSize: 20,color: Colors.red))
                  ],
                ),
                const SizedBox(height: 10),
                ElevatedButton(
                  onPressed: () {
                    // Add your button click logic here
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.black,
                    shape: const StadiumBorder(),
                    padding: const EdgeInsets.symmetric(horizontal: 120, vertical: 20)
                  ),
                  child: const Text('Checkout',style: TextStyle(fontSize: 24),),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}