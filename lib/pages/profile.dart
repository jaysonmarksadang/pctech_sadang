import 'package:flutter/material.dart';
import 'package:sadang_flutter/pages/sign_in.dart';

class Profile extends StatelessWidget {
  const Profile({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
       appBar: AppBar(
        backgroundColor: Colors.black,
        leading: IconButton(onPressed: (){Navigator.pop(context);}, icon: const Icon(Icons.arrow_back)),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
              Container(
                width: double.infinity,
                height: 290,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('lib/images/profile.png'),
                    fit: BoxFit.cover)),
              ),
              const SizedBox(height: 30),
              const Text('Mark Jayson Sadang',style: TextStyle(color: Colors.white,fontSize: 24),),
              const SizedBox(height: 30),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Row(
                  children:  [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                     Text('Email:',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20),),
                     SizedBox(height: 10),
                     Text('markjaysonsadang@gmail.com',style: TextStyle(color: Colors.white),),
                     SizedBox(height: 10),
                     Text('Number:',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20),),
                     SizedBox(height: 10),
                     Text('+63 9279373972',style: TextStyle(color: Colors.white),),
                     SizedBox(height: 10),
                     Text('Location',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20),),
                     SizedBox(height: 10),
                     Text('Cuyapo Nueva Ecija',style: TextStyle(color: Colors.white),),
                      ],
                    ),
                     
                  ],
                ),
              ),
              const SizedBox(height: 60),
              ElevatedButton(onPressed: (){
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Signin()));
              }, 
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.white,
                shape: const StadiumBorder(),
                padding: const EdgeInsets.symmetric(horizontal: 80,vertical: 15)
              ),
              child: const Text('Logout',style: TextStyle(color: Colors.black),)),
              const SizedBox(height: 30),
              ],
            ),
          ),
        )),
    );
  }
}