import 'package:flutter/material.dart';
import 'package:sadang_flutter/pages/cart.dart';

class Info extends StatelessWidget {
  const Info({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: IconButton(onPressed: (){Navigator.pop(context);}, icon: const Icon(Icons.arrow_back)),
        actions: [
          IconButton(onPressed: (){

          }, icon: const Icon(Icons.favorite_outline))
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(height: 20),
                Row(children: const [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    child: Text('HyperX Alloy Origins 60 \nMechanical Gaming Keyboard',style: TextStyle(fontSize: 18),),
                  )
                ],),
                Image.asset('lib/images/item1.png',
                height: 180,
                width: double.infinity,
                fit: BoxFit.cover,
                ),
                const SizedBox(height: 10),
                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 10.0,
                        height: 10.0,
                        decoration:  const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.black,
                        ),
                      ),
                      const SizedBox(width: 10.0),
                      Container(
                        width: 10.0,
                        height: 10.0,
                        decoration: const BoxDecoration(
                         
                          shape: BoxShape.circle,
                          
                          color: Colors.black,
                        ),
                      ),
                      const SizedBox(width: 10.0),
                      Container(
                        width: 10.0,
                        height: 10.0,
                        decoration: const BoxDecoration(
                         
                          shape: BoxShape.circle,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                   const SizedBox(height: 10),
                   
                  Container(
                    height: 480,
                    width: double.infinity,
                    decoration: const BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20)
                      )
                    ),child: Column(
                      children: [
                        const SizedBox(height: 20),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: const [
                              Text('Colors',style: TextStyle(color: Colors.white,fontSize: 20),),
                              Text('Select Model',style: TextStyle(color: Colors.white,fontSize: 20))
                            ],
                          ),
                        ),
                        const SizedBox(height: 10),

                      Row(mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 20),
                              child: Row(
                                children:  [
                                 Container(
                                  padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                                  decoration: BoxDecoration(color: Colors.white,
                                  borderRadius: BorderRadius.circular(18)
                                  ),child: const Text('Black',style: TextStyle(color: Colors.black),),
                                 ),
                                 const SizedBox(width: 10,),
                                 Container(
                                  padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                                  decoration: BoxDecoration(color: Colors.white,
                                  borderRadius: BorderRadius.circular(18)
                                  ),child: const Text('White',style: TextStyle(color: Colors.black),),
                                 )
                                ],
                              ),
                            ),
                             Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 20),
                              child: Row(
                                children:  [
                                 Container(
                                  padding:const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                                  decoration: BoxDecoration(color: Colors.white,
                                  borderRadius: BorderRadius.circular(18)
                                  ),child: const Text('Full Size',style: TextStyle(color: Colors.black),),
                                 ),
                                 const SizedBox(width: 10,),
                                 Container(
                                  padding:const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                                  decoration: BoxDecoration(color: Colors.white,
                                  borderRadius: BorderRadius.circular(18)
                                  ),child:const Text('60 Keys',style: TextStyle(color: Colors.black),),
                                 )
                                ],
                              ),
                            ),
                        ],
                      ),
                      const SizedBox(height: 20),
                      Padding(padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                                  padding:const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                                  decoration: BoxDecoration(color: Colors.white,
                                  borderRadius: BorderRadius.circular(18)
                                  ),child: const Text('Random',style: TextStyle(color: Colors.black),),
                                 ),
                                 Container(
                                  padding:const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                                  decoration: BoxDecoration(color: Colors.white,
                                  borderRadius: BorderRadius.circular(18)
                                  ),child:const Text('Tenkeyless',style: TextStyle(color: Colors.black),),
                                 )
                        ],
                      ),
                      ),
                      const SizedBox(height: 50),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: const [
                            Text('Details',style: TextStyle(fontSize: 20,color: Colors.white),),
                          ],
                        ),
                      ),
                      const SizedBox(height: 30),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                          Text('- Petite 60% form factor',style: TextStyle(fontSize: 16,color: Colors.white)),
                          SizedBox(height: 5),
                          Text('- HyperX mechanical switches',style: TextStyle(fontSize: 16,color: Colors.white)),
                          SizedBox(height: 5),
                          Text('- Double shot PBT keycaps (Black)',style: TextStyle(fontSize: 16,color: Colors.white)),
                          SizedBox(height: 5),
                          Text('- Customizable with HyperX NGENUITY Software',style: TextStyle(fontSize: 16,color: Colors.white)),
                        ],
                      ),
                      const SizedBox(height: 70),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                                  Container(
                                  padding:const EdgeInsets.symmetric(horizontal: 25,vertical: 15),
                                  decoration: BoxDecoration(color: Colors.white,
                                  borderRadius: BorderRadius.circular(25)
                                  ),child:const Text('P 4,990',style: TextStyle(color: Colors.black),),
                                 ),
                                 GestureDetector(
                                  onTap: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=>Cart()));
                                  },
                                   child: Container(
                                    padding:const EdgeInsets.symmetric(horizontal: 25,vertical: 15),
                                    decoration: BoxDecoration(color: Colors.white,
                                    borderRadius: BorderRadius.circular(25)
                                    ),child:const Text('Add to cart',style: TextStyle(color: Colors.black),),
                                   ),
                                 ),
                                 Container(
                                  padding:const EdgeInsets.symmetric(horizontal: 25,vertical: 15),
                                  decoration: BoxDecoration(color: Colors.white,
                                  borderRadius: BorderRadius.circular(25)
                                  ),child:const Text('Buy Now',style: TextStyle(color: Colors.black),),
                                 )
                        ],
                      ),
                      const SizedBox(height: 20),

                      ],
                    ),
                  ),
                  
              ],
            ),
          ),
        )),
    );
  }
}