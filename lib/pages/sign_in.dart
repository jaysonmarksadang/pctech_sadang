import 'package:flutter/material.dart';
import 'package:sadang_flutter/pages/home.dart';
import 'package:sadang_flutter/pages/sign_up.dart';

import '../components/textfield.dart';

class Signin extends StatelessWidget {
  Signin({super.key});

final usernameController = TextEditingController();

final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 40),
                  child: Image.asset('lib/images/splash.png',height: 200,width: double.infinity,fit: BoxFit.fill,),
                ),
                
                const Text('Sign in',style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                const SizedBox(height: 20),
              
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Container(
                    width: double.infinity,
                    decoration:  BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(20)
                    ),child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children:  [
                        const SizedBox(height: 30,),
                        MyTextField(controller: usernameController, obscureText: false, hintText: 'Username', prefixIcon: const Icon(Icons.account_circle),),
                        const SizedBox(height: 30),
                        MyTextField(controller: passwordController, obscureText: true, hintText: 'Password', prefixIcon: const Icon(Icons.lock),),
                        const SizedBox(height: 40,),
                        ElevatedButton(onPressed: (){
                          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> Home()));
                        },
                        style: ElevatedButton.styleFrom(
                          shape: const StadiumBorder(),
                          backgroundColor: Colors.white,
                          padding: const EdgeInsets.symmetric(horizontal: 90,vertical: 15)
                        ),
                         child: const Text('Log in',style: TextStyle(color: Colors.black,fontSize: 20),)),

                        const SizedBox(height: 30,),
                        ElevatedButton(onPressed: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=> Signup()));
                        },
                        style: ElevatedButton.styleFrom(
                          shape: const StadiumBorder(),
                          backgroundColor: Colors.white,
                          padding: const EdgeInsets.symmetric(horizontal: 40,vertical: 15)
                        ),
                         child: const Text('Sign up',style: TextStyle(color: Colors.black),)),
                          const SizedBox(height: 30,)

                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 60,),
              ],
            ),
          ),
        )
      ),
    );
  }
}