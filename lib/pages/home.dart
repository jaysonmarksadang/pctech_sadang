import 'package:flutter/material.dart';
import 'package:sadang_flutter/components/item_container.dart';
import 'package:sadang_flutter/components/textfield.dart';
import 'package:sadang_flutter/pages/cart.dart';
import 'package:sadang_flutter/pages/item_info.dart';
import 'package:sadang_flutter/pages/profile.dart';

class Home extends StatelessWidget {
 Home({super.key});

final searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(80),
        child: SafeArea(
          child: AppBar(automaticallyImplyLeading: false,
            toolbarHeight: 80,
            backgroundColor: Colors.black,
            title: null,
            actions: [
              IconButton(
                
                icon: const Icon(Icons.shopping_cart_outlined,size: 40,),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>const Cart()));
                },
              ),
            ],
            centerTitle: true,
            flexibleSpace: Container(
              padding: const EdgeInsets.fromLTRB(30,10,55,0),
              child: TextField(
                style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
                decoration: InputDecoration(
                  prefixIcon: const Icon(
                    Icons.search,
                    color: Colors.black,
                  ),
                  hintText: 'Search',
                  hintStyle: const TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
                  filled: true,
                  fillColor: Colors.white,
                  
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0),
                    borderSide: BorderSide.none,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                const SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: SizedBox(
                  width: double.infinity,
                  height: 160,
                    child: Stack(
                      fit: StackFit.expand,
                      children: [
                      Image.asset(
                      'lib/images/topimg.png', // Replace with your own image path
                      fit: BoxFit.fill,
                    ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                    padding: const EdgeInsets.only(bottom: 16.0),
                    child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 10.0,
                        height: 10.0,
                        decoration:  BoxDecoration(
                          border: Border.all(color: Colors.white),
                          shape: BoxShape.circle,
                          color: Colors.black,
                        ),
                      ),
                      const SizedBox(width: 10.0),
                      Container(
                        width: 10.0,
                        height: 10.0,
                        decoration:  BoxDecoration(
                          border: Border.all(color: Colors.white),
                          shape: BoxShape.circle,
                          
                          color: Colors.black,
                        ),
                      ),
                      const SizedBox(width: 10.0),
                      Container(
                        width: 10.0,
                        height: 10.0,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          shape: BoxShape.circle,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                              ),
                            ),
                          ],
                        ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children:  [
                      GestureDetector(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>const Info()));
                        },
                        child: const ItemContainer(imagePath: 'lib/images/item1.png',inputText: 'HyperX Alloy Origins 60 Mechanical Gaming Keyboard', inputPrice: 'P 4,990',inputSold: '6k Sold',)),
                      const ItemContainer(imagePath: 'lib/images/item2.png',inputText: 'MSI Optix G241VC', inputPrice: 'P 8,395',inputSold: '5k Sold'),
                    ],
                  ),
                ),
                const SizedBox(height: 10),
                 Padding(
                   padding: const EdgeInsets.symmetric(horizontal: 30),
                   child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: const [
                      ItemContainer(imagePath: 'lib/images/item3.png',inputText: 'Gigabyte GeForce RTX 2060 OC 6GB GDDR6', inputPrice: 'P 34,999',inputSold: '2k Sold'),
                      ItemContainer(imagePath: 'lib/images/item4.png',inputText: 'DEEPCOOL UL551A RGB AIR CPU COOLER', inputPrice: 'P 1,500',inputSold: '6k Sold'),
                    ],
                                 ),
                 ),
                 const SizedBox(height: 10),
                 Padding(
                   padding: const EdgeInsets.symmetric(horizontal: 30),
                   child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: const [
                      ItemContainer(imagePath: 'lib/images/item5.png',inputText: 'Armaggeddon Nimitz TR 8000 Extended ATX Gaming PC Case', inputPrice: 'P 37,000',inputSold: '3k Sold'),
                      ItemContainer(imagePath: 'lib/images/item6.png',inputText: 'Logitech G502', inputPrice: 'P 2,850',inputSold: '8k Sold'),
                    ],
                                 ),
                 )
              ],
            ),
          ),
        )),
        bottomNavigationBar: BottomAppBar(
          
      color: Colors.black,
      child: Padding(
        padding: const EdgeInsets.all(0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children:  [
            IconButton(onPressed: (){
              
            }, icon: const Icon(Icons.home_outlined,size: 40,color: Colors.white,)),
            IconButton(onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>const Profile()));
            }, icon: const Icon(Icons.account_circle_outlined,size: 40,color: Colors.white))
          ],
        ),
      ),
    ),
    );
  }
}